﻿using System;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Xml.Linq;
using Newtonsoft.Json;

namespace opendata.Classes
{
    public enum OutputType : ushort
    {
        JsonReader = 0,
        XML = 1
    }

    public class CJsonWebService
    {
        public CJsonWebService()
        {
        }

        //Get from Web Service (Topics) in Object Oriented format
        public static TopicsRoot RunTopicsWebService(string url)
        {
            TopicsRoot allTopics = null;

            using (WebClient client = new WebClient())
            {
                CJsonTopics json = new CJsonTopics(client.DownloadString(url));
                allTopics = json.GetJson();
            }

            return allTopics;
        }

        //Can get from Web Service (Topics) in JsonReader or XML format
        public static object RunTopicsWebService(string url,OutputType type)
        {
            object returnObj = null;

            using (WebClient client = new WebClient())
            {
                string json = client.DownloadString(url);
                
                if(type == OutputType.XML)
                    returnObj = JsonConvert.DeserializeXNode(json, "Root");
                else
                    returnObj = new JsonTextReader(new StringReader(json));

            }

            return returnObj;
        }

        public static Datasets RunDatasetsWebService(string url)
        {
            Datasets allDatasets = null;

            using (WebClient client = new WebClient())
            {
                CJsonDatasets json = new CJsonDatasets(client.DownloadString(url));
                allDatasets = json.GetJson();
            }

            return allDatasets;
        }

        public static object RunDatasetsWebService(string url, OutputType type)
        {
            object returnObj = null;
            //OutputType.JsonReader

            using (WebClient client = new WebClient())
            {
                string json = client.DownloadString(url);

                if (type == OutputType.XML)
                    returnObj = JsonConvert.DeserializeXNode(json, "Root");
                else
                    returnObj = new JsonTextReader(new StringReader(json));
            }

            return returnObj;
        }

        public static DatasetDetails RunDatasetDetailsWebService(string url)
        {
            DatasetDetails allDatasets = null;

            using (WebClient client = new WebClient())
            {
                CJsonDatasetDetails json = new CJsonDatasetDetails(client.DownloadString(url));
                allDatasets = json.GetJson();
            }

            return allDatasets;
        }

        public static object RunDatasetDetailsWebService(string url, OutputType type)
        {
            object returnObj = null;

            using (WebClient client = new WebClient())
            {
                string json = client.DownloadString(url);

                if (type == OutputType.XML)
                    returnObj = JsonConvert.DeserializeXNode(json, "Root");
                else
                    returnObj = new JsonTextReader(new StringReader(json));
            }

            return returnObj;
        }
    }
}

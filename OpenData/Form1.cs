﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OpenData
{
    public partial class Form1 : Form
    {
        public View View{ get; set; }
        public Form1()
        {
            InitializeComponent();
        }

        private void AreaOfStudy_SelectedIndexChanged(object sender, EventArgs e)
        {
            View.EscolherArea(sender,e);
        }

        private void AreaThemes_SelectedIndexChanged(object sender, EventArgs e)
        {
            View.EscolherTema(sender, e);
        }

        public void ShowError()
        {
            MessageBox.Show("Ocorreu um erro");
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace OpenData
{
    public class Controller
    {
		// cria e reserva espaço para os objetos model e view
		Model model;
		View view;

        opendata.Classes.CJsonWebService webService;

		// 1 - definir o delegate
		// 2 - definir o evento
		// 3 - fazer o raise fo evento

		// quando foram recebidos dados para escolha de áeras de estudo ( grande nivel )
		public delegate void buscadasAreasEventHandler(object source, EventArgs args);

		// quando foram recebidos dados para tratamento ( temas e detalhes )
		public delegate void buscaddosTemasEventHandler(object source, EventArgs args);

		// quando foram recebidos dados para tratamento ( temas e detalhes )
		public delegate void buscadosDadosEventHandler(object source, EventArgs args);

		// define os eventos
		public event buscadasAreasEventHandler buscadasAreas;
		public event buscaddosTemasEventHandler buscadosTemas;
		public event buscadosDadosEventHandler buscadosDados;

		public Controller()
		{
			view = new View(model);          // instancia o view, passando-lhe o model
			model = new Model(this, view);          // instancia o model, passando-he o view
											  // o view necessita de conhecer o model e vice-versa

			webService = new opendata.Classes.CJsonWebService();

			// Definido como escolhidoTema o evento a ser levandtado pelo view quando o utilizador
			// escolhe o tema específico que pretende ver. Definido como OnEscolhidoTema o método
			// no model, consumidor deste evento.  Alterar se necessário e descomentar as linhas.
			view.UserEscolheuNovoTema += UserEscolheuNovoTema;

			// Definidos como buscarAreas e escolhidaArea os eventos a levantar pelo view,
			// quando o utilizador informa que quer ver as áreas( grandes tópicos) para escolha e informa
			// a grane área escolhida, para ver o tema ou tópico de cada uma das áreas.
			// Alterar se necessário e descomentar as linhas.
			//view.escolhidaArea += this.OnEscolhidaArea;
			view.UserEscolheuNovaArea += UserEscolheuNovaArea;
			// TODO: implementar o ir buscar áreas
			view.buscarAreas += buscarAreas;


			// TODO: Definir aqui as subscrições a eventos, para isso é necessário saber os eventos

			// Definidos como OnBuscadasAreas e OnBuscadosDados os dois métodos do Model
			// responsáveis por consumir esses dois eventos. Alterar se necessário e descomentar as linhas.
			//this.buscadasAreas += model.OnBuscadasAreas;
			//this.buscadosDados += model.OnBuscadosDados;

			// definidos os eventos do model que informam mudança de estado relativamente a dados tratados,
			// grandes áreas ou tópicos formatados para escolha, temas formatados para escolha e
			// dados formatados para visuaçlização, bem como os eventos no view associados a eles.
			//Alterar se necessário e descomentar as linhas.
			//model.topicosProntos += view.OnTopicosProntos;
			//model.temasProntos += view.OnTemasProntos;
			//model.dadosProntos += view.OnDadosProntos;

		}

		// deverá ativar o interface
		public void Iniciar()
		{
			// TODO: ativar o interface no view
			view.AtivarInterface();
		}

		// vai buscar a lista de topicos / areas de estudo 
		public void BuscarTopicosHTTP()
		{
			string url = "";
			// TODO: invocar o webservice externo -> passar a lista de topicos ao model para processar			


			OnBuscadasAreas();

		}

		// vai buscar temas de uma área escolhida
		public void BuscarDadosHTTP(string area)
		{
			string url = "";
			// TODO: construir a string url para incluir o id da área

			// TODO: invocar o webservice externo -> passar os dados para o model organizar por temas

			// vai invocar o método que faz o raise do event.
			// Pode ser passada a lista como parametro do método, caso contrário a lista 
			// terá que ser global
			OnBuscadosTemas();
		}

		// vai buscar detalhes de um tema
		public void BuscarDetailsHTTP(string tema)
		{
			string url = "";
			//view._form.AreaOfStudy.DataSource = Areas.Area;
			// TODO: invocar o webservice externo -> passar a lista de topicos ao model para processar

			// vai invocar o método que faz o raise do event.
			// Pode ser passada a lista como parametro do método, caso contrário a lista 
			// terá que ser global
			OnBuscadosDados();
		}

		public void buscarAreas(object sender, EventArgs e)
        {
			BuscarTopicosHTTP();

		}

		public void UserEscolheuNovaArea(object sender, EventArgs e)
        {
			string areaEscolhida = "";

			//TODO: retirar a string da área escolhida pelo utilizador
			// para passar ao método BuscarDadosHTTP.
			BuscarDadosHTTP(areaEscolhida);
		}

		public void UserEscolheuNovoTema(object sender, EventArgs e)
		{
			string temaEscolhido = "";


			BuscarDetailsHTTP(temaEscolhido);
		}

		// método que irá fazer o raise do event
		protected virtual void OnBuscadasAreas()
		{

			// verifica se o evento tem subscritores
			if (buscadasAreas != null)
			{
				// TODO: substituir EventArgs.Empty pelos dados recebidos para
				// serem tratados pelo model
				buscadasAreas(this, EventArgs.Empty);
			}

		}

		// método que irá fazer o raise do event
		protected virtual void OnBuscadosTemas()
		{

			// verifica se o evento tem subscritores
			if (buscadosTemas != null)
			{
				// TODO: substituir EventArgs.Empty pelos dados recebidos para
				// serem tratados pelo model
				buscadosTemas(this, EventArgs.Empty);
			}

		}

		// método que irá fazer o raise do event
		protected virtual void OnBuscadosDados()
		{

			// verifica se o evento tem subscritores
			if (buscadosDados != null)
			{
				// TODO: substituir EventArgs.Empty pelos dados recebidos para
				// serem tratados pelo model
				buscadosDados(this, EventArgs.Empty);
			}

		}
	}
}

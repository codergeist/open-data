﻿using OpenData.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenData
{

		public interface IArea
		{
			string Descricao { get; set; }
			string id { get; set; }
		}

		public interface ITema
        {
			string Descricao { get; set; }
			string id { get; set; }
		}

		public class Model
		{
			private View view;
			private Controller controller;
			public event NotificaTopicosProntos topicosProntos;
			public event NotificaTemasProntos temasProntos;
			public event NotificaDadosProntos dadosProntos;

			public delegate void NotificaTopicosProntos(List<Area> Area);
			public delegate void NotificaTemasProntos(List<Tema> tema);
			public delegate void NotificaDadosProntos(List<String>dados);



			// TODO: tipos de dados necessários
			// atenção: o model estará em diversos estados, vazio, com áreas, com lista de temas, com dados formatados
			// poderá fazer sentido implementar uma máquina de estados.






			// construtor que recebe o view como parametro
			public Model(Controller c, View v)
			{
				controller = c;
				view = v;


				controller.buscadasAreas += processarAreas;
				controller.buscadosTemas += processarTemas;
				controller.buscadosDados += processarDados;

			}

			// recebe a lista de áreas do controller, processa os dados e organiza, despoletando depois
			// um evento o qual irá informar o view que deve pedir atualização. a atualização será
			// a lista de áreas para escolha
			// TODO: definir o formato dos dados de entrada ( resultantes da invocação do webservice externo )
			// List<string> dados é um exemplo aceite pelo compilador
			public void processarAreas(object sender, EventArgs e)
			{
				var dados = new List<Area>();
				/*dynamic dynJson = JsonConvert.DeserializeObject(dados);
				foreach (var column in dynJson)
				{

				}*/
				topicosProntos(dados);
				// TODO: formatar a lista de áreas, atualizar estado e levantar o evento

			}

			// recebe dados nos quais extrai os temas dentro da área escolhida, despoletando depois
			// um evento o qual irá informar o view que deve pedir atualização.
			// TODO: definir o formato dos dados de entrada ( resultantes da invocação do webservice externo )
			// List<string> dados é um exemplo aceite pelo compilador
			public void processarTemas(object sender, EventArgs e)
			{
				var dados = new List<Tema>();
				/*dynamic dynJson = JsonConvert.DeserializeObject(dados);
				foreach (var column in dynJson)
				{

				}*/

				temasProntos(dados);
				// TODO: formatar os temas dentro da área escolhida, atualizar o estado e levantar o evento
			}

			// formatar dados do tema escolhido para apresentação
			// TODO: definir o formato em que recebe o tema escolhido ( exemplo, string com o nome do tema )
			// List<string> dados é um exemplo aceite pelo compilador
			public void processarDados(object sender, EventArgs e)
			{
				var dados = new List<string>();
				/*dynamic dynJson = JsonConvert.DeserializeObject(dados);
				foreach (var column in dynJson)
				{

				}*/
				dadosProntos(dados);
				// TODO: formatar os dados para apresentação, atualizar o estado  e levantar o evento
			}

			public void SolicitacaoArea(string area)
			{

				// TODO: formatar os dados para apresentação, atualizar o estado  e levantar o evento
			}

			// volta ao estado anterior. Exemplo, foi escolhido um tema, foram mostrados os dados, 
			// o utilizador clica em "voltar" para voltar à lista de temas para escolher outro tema
			public void Voltar()
			{
				// TODO: voltar um estado atrás e levantar o evento para atualização

			}
		}

}

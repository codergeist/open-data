﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenData.Models.WS
{
    public class TemaWS : ITema
    {
        public string Descricao { get; set; }
        public string id { get; set; }
    }
}

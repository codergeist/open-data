﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace opendata.Classes
{
    public class Owner
    {
        public string avatar { get; set; }
        public object avatar_thumbnail { get; set; }
        public string @class { get; set; }
        public string first_name { get; set; }
        public string id { get; set; }
        public string last_name { get; set; }
        public string page { get; set; }
        public string slug { get; set; }
        public string uri { get; set; }
    }

    /*public class Datum
    {
        public object created_at { get; set; }
        public List<object> datasets { get; set; }
        public object deleted { get; set; }
        public string description { get; set; }
        public bool featured { get; set; }
        public string id { get; set; }
        public object last_modified { get; set; }
        public string name { get; set; }
        public Owner owner { get; set; }
        public string page { get; set; }
        public bool @private { get; set; }
        public List<object> reuses { get; set; }
        public string slug { get; set; }
        public List<string> tags { get; set; }
        public string uri { get; set; }
    }*/

    public class TopicsRoot
    {
        public List<Datum> data { get; set; }
        public object facets { get; set; }
        public object next_page { get; set; }
        public int page { get; set; }
        public int page_size { get; set; }
        public object previous_page { get; set; }
        public int total { get; set; }
    }

    public class CJsonTopics
    {
        private TopicsRoot myDeserializedClass;

        public CJsonTopics()
        {
            
        }

        public CJsonTopics(string jsonResponse)
        {
            myDeserializedClass = JsonConvert.DeserializeObject<TopicsRoot>(jsonResponse);
        }

        public TopicsRoot GetJson()
        {
            return myDeserializedClass;
        }
    }
}

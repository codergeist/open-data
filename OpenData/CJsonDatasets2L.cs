﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace opendata.Classes
{
    public class DatasetDetails
    {
        public object acronym { get; set; }
        public object archived { get; set; }
        public List<object> badges { get; set; }
        public DateTime created_at { get; set; }
        public object deleted { get; set; }
        public string description { get; set; }
        public Extras extras { get; set; }
        public string frequency { get; set; }
        public object frequency_date { get; set; }
        public string id { get; set; }
        public DateTime last_modified { get; set; }
        public DateTime last_update { get; set; }
        public string license { get; set; }
        public Metrics metrics { get; set; }
        public Organization organization { get; set; }
        public object owner { get; set; }
        public string page { get; set; }
        public bool @private { get; set; }
        public List<Resource> resources { get; set; }
        public string slug { get; set; }
        public Spatial spatial { get; set; }
        public List<string> tags { get; set; }
        public object temporal_coverage { get; set; }
        public string title { get; set; }
        public string uri { get; set; }
    }

    public class CJsonDatasetDetails
    {
        DatasetDetails myDeserializedClass;

        public CJsonDatasetDetails(string jsonResponse)
        {
            myDeserializedClass = JsonConvert.DeserializeObject<DatasetDetails>(jsonResponse);
        }

        public DatasetDetails GetJson()
        {
            return myDeserializedClass;
        }
    }
}

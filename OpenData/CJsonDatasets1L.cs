﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace opendata.Classes
{
    public class Extras
    {
        [JsonProperty("check:available")]
        public bool CheckAvailable { get; set; }

        [JsonProperty("check:count-availability")]
        public int CheckCountAvailability { get; set; }

        [JsonProperty("check:date")]
        public DateTime CheckDate { get; set; }

        [JsonProperty("check:status")]
        public int CheckStatus { get; set; }
    }

    public class Metrics
    {
        public int discussions { get; set; }
        public int followers { get; set; }
        public int issues { get; set; }
        public int reuses { get; set; }
        public int views { get; set; }
    }

    public class Organization
    {
        public string acronym { get; set; }
        public string @class { get; set; }
        public string id { get; set; }
        public string logo { get; set; }
        public string logo_thumbnail { get; set; }
        public string name { get; set; }
        public string page { get; set; }
        public string slug { get; set; }
        public string uri { get; set; }
    }

    public class Checksum
    {
        public string type { get; set; }
        public string value { get; set; }
    }

    public class Resource
    {
        public Checksum checksum { get; set; }
        public DateTime created_at { get; set; }
        public string description { get; set; }
        public Extras extras { get; set; }
        public int? filesize { get; set; }
        public string filetype { get; set; }
        public string format { get; set; }
        public string id { get; set; }
        public DateTime last_modified { get; set; }
        public string latest { get; set; }
        public Metrics metrics { get; set; }
        public string mime { get; set; }
        public object preview_url { get; set; }
        public DateTime published { get; set; }
        public string title { get; set; }
        public string type { get; set; }
        public string url { get; set; }
    }

    public class Spatial
    {
        public object geom { get; set; }
        public string granularity { get; set; }
        public List<string> zones { get; set; }
    }

    public class TemporalCoverage
    {
        public string end { get; set; }
        public string start { get; set; }
    }

    public class Datum
    {
        public object acronym { get; set; }
        public object archived { get; set; }
        public List<object> badges { get; set; }
        public DateTime created_at { get; set; }
        public object deleted { get; set; }
        public string description { get; set; }
        public Extras extras { get; set; }
        public string frequency { get; set; }
        public DateTime frequency_date { get; set; }
        public string id { get; set; }
        public DateTime last_modified { get; set; }
        public DateTime last_update { get; set; }
        public string license { get; set; }
        public Metrics metrics { get; set; }
        public Organization organization { get; set; }
        public object owner { get; set; }
        public string page { get; set; }
        public bool @private { get; set; }
        public List<Resource> resources { get; set; }
        public string slug { get; set; }
        public Spatial spatial { get; set; }
        public List<string> tags { get; set; }
        public TemporalCoverage temporal_coverage { get; set; }
        public string title { get; set; }
        public string uri { get; set; }
    }

    public class Facets
    {
    }

    public class Datasets
    {
        public List<Datum> data { get; set; }
        public Facets facets { get; set; }
        public string next_page { get; set; }
        public int page { get; set; }
        public int page_size { get; set; }
        public object previous_page { get; set; }
        public int total { get; set; }
    }

    public class CJsonDatasets
    {
        Datasets myDeserializedClass;

        public CJsonDatasets(string jsonResponse)
        {
            myDeserializedClass = JsonConvert.DeserializeObject<Datasets>(jsonResponse); 
        }

        public Datasets GetJson()
        {
            return myDeserializedClass;
        }
    }
}

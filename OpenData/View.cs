﻿using System;
using OpenData.Models;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OpenData
{
    public class View
    {
		private Model model;
		public event EventHandler buscarAreas;
		public event EventHandler UserEscolheuNovaArea;
		public event EventHandler UserEscolheuNovoTema;
		public event EventHandler UserClickVoltar; 
		public event SolicitacaoError ViewError;
		public delegate string SolicitacaoError();
		public delegate void SolicitacaoArea(ref List<string> ListaAreas);
		public delegate void SolicitacaoTemas(ref List<string> ListaTema);
		public Form1 _form;
		// TODO: criar a windows form na qual iremos comunicar com o utilizador
		// TODO: criar outras variáveis necessárias se aplicável

		// TODO: criação de eventos 
		// TODO: criação de delegados

		// construtor recebe o model
		public View(Model m)
		{
			model = m;

			model.topicosProntos += mostrarAreas;
			model.temasProntos += mostrarTemas;
			model.dadosProntos += mostrarDados;
		}

		public void AtivarInterface()
		{
			// TODO: desenhar o interface inicial
			_form = Application.OpenForms.OfType<Form1>().FirstOrDefault() ?? new Form1();
			_form.View = this;
			_form.ShowDialog();
			BuscarArea(this, null);

		}

		public void BuscarArea(object sender, EventArgs e)
		{
			buscarAreas(this, e);
		}

		public void EscolherArea(object sender, EventArgs e)
		{
			// TODO: vai informar o controller da área escolhida
			UserEscolheuNovaArea(this, e);
			
		}

		public void BuscarTemas(object sender,EventArgs e)
        {
			//
        }

		public void EscolherTema(object sender, EventArgs e)
		{
			// TODO: vair informar o controller do tema escolhido
			UserEscolheuNovoTema(this, e);
		}
		public void Error()
		{
			_form.ShowError();
		}

		public void Voltar(object sender, EventArgs e)
		{
			UserClickVoltar(sender, e);
			// TODO: pede ao model para voltar par ao estado anterior
		}

		public void mostrarAreas(List<Area> areas)
        {
			//TODO: mostrar as áreas para escolha
        }

		public void mostrarTemas(List<Tema> areas)
		{
			//TODO: mostrar os temas para escolha
		}

		public void mostrarDados(List<String> areas)
		{
			//TODO: mostrar os dados detail para análise
		}


		// TODO: quando o view recebe a informação do model que houve uma alteração de estado,
		// deve mostrar os dados conforme o novo estado, por exemplo, se o novo estado for áreas
		// para escolha, então deverá mostrar a lista de áreas para escolher, se for temas para escolha,
		// deverá mostrar a lista de temas parar o utilizador escolher. Se o estado atual for de dados
		// acerca de um tema, então o view deve mostrar os dados e as únicas opções em termos de fluxo serão
		// voltar e sair.
		// Isto poderá ser implementado em apenas um método ou por exemplo em três métodos distintos, os quais
		// serão invocados por um agregador, o qual decide que método invocar com base no estado.
		// Exemplo: estado 1 - áreas -> invoca o método para mostrar e escolher as áreas de estudo.

	}
}

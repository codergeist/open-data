﻿using OpenData.Models;
using OpenData.Models.WS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenData.Repositories
{
    public static class TemaRepository
    {
        public static List<Tema> Converter(List<TemaWS> modelWs)
        {
            var model = new List<Tema>();

            foreach (var item in modelWs)
            {
                model.Add(Converter(item));
            }
            return model;
        }

        private static Tema Converter(TemaWS temaWS)
        {
            var model = new Tema();

            model.Descricao = temaWS.Descricao;
            model.Id = temaWS.Id;
            
            return model;
        }
    }
}

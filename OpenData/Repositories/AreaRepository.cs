﻿using OpenData.Models;
using OpenData.Models.WS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenData.Repositories
{
    public static class AreaRepository
    {
        public static List<Area> Converter(List<AreaWS> modelWs)
        {
            var model = new List<Area>();

            foreach (var item in modelWs)
            {
                model.Add(Converter(item));
            }
            return model;
        }

        private static Area Converter(AreaWS temaWS)
        {
            var model = new Area();

            model.Descricao = temaWS.Descricao;
            model.id = temaWS.id;

            return model;
        }
    }

}

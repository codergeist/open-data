﻿
namespace OpenData
{
    partial class Form1
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.AreaOfStudy = new System.Windows.Forms.ComboBox();
            this.AreaThemes = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // AreaOfStudy
            // 
            this.AreaOfStudy.FormattingEnabled = true;
            this.AreaOfStudy.Location = new System.Drawing.Point(58, 52);
            this.AreaOfStudy.Name = "AreaOfStudy";
            this.AreaOfStudy.Size = new System.Drawing.Size(121, 23);
            this.AreaOfStudy.TabIndex = 0;
            this.AreaOfStudy.SelectedIndexChanged += new System.EventHandler(this.AreaOfStudy_SelectedIndexChanged);
            // 
            // AreaThemes
            // 
            this.AreaThemes.FormattingEnabled = true;
            this.AreaThemes.Location = new System.Drawing.Point(58, 112);
            this.AreaThemes.Name = "AreaThemes";
            this.AreaThemes.Size = new System.Drawing.Size(121, 23);
            this.AreaThemes.TabIndex = 1;
            this.AreaThemes.Visible = false;
            this.AreaThemes.SelectedIndexChanged += new System.EventHandler(this.AreaThemes_SelectedIndexChanged);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.AreaThemes);
            this.Controls.Add(this.AreaOfStudy);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);

        }

        #endregion

        public System.Windows.Forms.ComboBox AreaOfStudy;
        public System.Windows.Forms.ComboBox AreaThemes;
    }
}

